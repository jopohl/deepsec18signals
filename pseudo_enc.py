#!/usr/bin/env python3

import sys

import numpy as np

bits = sys.argv[-1]

sync_seq = "11101001110010101110100111001010"

sync_start = bits.find(sync_seq)
if sync_start == -1 or sync_start < 32:
    sys.exit(1)  # Sync not found or preamble too short

# Pack to bytes
packed = np.array([int(bits[i:i + 8], 2) for i in range(sync_start - 32, 8 * (len(bits) // 8), 8)], dtype=np.uint8)

result = np.array(packed, dtype=np.uint8)

offset = 8  # 8 byte preamble + sync
result[offset + 0] = packed[offset + 0]
result[offset + 1] = (~packed[offset + 1]) ^ 0x89

if sys.argv[1] == "d":
    for i in range(offset + 2, len(packed)):
        result[i] = (packed[i - 1] + 0xdc) ^ packed[i]
    result[-3] = packed[-3] ^ result[offset + 2]
else:
    for i in range(offset + 2, len(packed)):
        result[i] = (result[i - 1] + 0xdc) ^ packed[i]
    result[-3] = packed[-3] ^ packed[offset + 2]

# Set CRC to 0
result[-2] = 0
result[-1] = 0

# Keep preamble
print("".join(["1010" for _ in range(32, 4 * (sync_start // 4), 4)]), end="")

# print binary
print("".join(["{:08b}".format(b) for b in result]), end="")
